import { OpenoceanApiSdk } from "@openocean.finance/api";
import { ReqSwapVo } from "@openocean.finance/api/lib/swapSdk/Swap";

export class OpenOceanLib {
  openOceanApiSdk: any;
  chain: string;
  walletName: string;
  wallet: any;

  constructor(chain: string, walletName: string) {
    console.log("initializing");
    this.chain = chain;
    this.walletName = walletName;
    this.openOceanApiSdk = new OpenoceanApiSdk();
  }
  public getWallet() {
    return this.wallet;
  }

  public connectWallet = async () => {
    const { swapSdk } = this.openOceanApiSdk;
    const { code, sdk } = await swapSdk.connectWallet({
      chain: this.chain,
      walletName: this.walletName,
    });
    if (code != 200) throw "can not connect wallet";
    this.wallet = sdk.wallet;
    return this.wallet;
  };

  public getGasPrice = async (chain?: string) => {
    if (!chain) chain = this.chain;
    const { api } = this.openOceanApiSdk;
    const data = await api.getGasPrice({ chain });
    if (data.code == 200) {
      return data.data.standard as string;
    }
    throw "can not get gas price";
  };

  public getTokenList = async (chain?: string) => {
    if (!chain) chain = this.chain;
    const { api } = this.openOceanApiSdk;
    const { data } = await api.getTokenList({ chain });
    return data;
  };

  public getQuote = async (
    inTokenAddress: string,
    outTokenAddress: string,
    amount: Number,
    gasPrice: string,
    slippage: Number
  ) => {
    const { api } = this.openOceanApiSdk;
    const response = await api.quote({
      chain: this.chain,
      inTokenAddress,
      outTokenAddress,
      amount,
      gasPrice,
      slippage,
    });
    if (response.code == 200) {
      return response.data;
    }
    throw "can not get quote";
  };
  public swap = async (req: ReqSwapVo) => {
    const { swapSdk } = this.openOceanApiSdk;
    const response = await swapSdk.swapQuote(req);
    if (response.code == 200) {
      swapSdk
        .swap(response.data)
        .on("error", (err: Error) => {
          console.log("error: ", err);
          throw `swap err: ${err}`;
        })
        .on("transanctionHash", (hash: string) => {
          console.log("tx hash:", hash);
        })
        .on("receipt", (data: any) => {
          console.log("receipd:", data);
        })
        .on("success", (data: any) => {
          console.log("success:", data);
          return data;
        });
    }
    throw "can not swap";
  };
}
