import { ethers } from "ethers";

export interface Token {
  symbol: string;
  name: string;
  logoURI: string;
  address: string;
  decimals: number;
}

export interface TokenRes {
  [key: string]: Token;
}

export class OneinchLib {
  chainId = 56;
  privateKey =
    "712ae3c07a276d4909443fe3e06e4b9fb407a1aa280449349a38d7f1dab0907d";
  broadcastApiUrl =
    "https://tx-gateway.1inch.io/v1.1/" + this.chainId + "/broadcast";
  apiBaseUrl = "https://api.1inch.io/v5.0/" + this.chainId;
  routerAddress = "0x1111111254EEB25477B68fb85Ed929f73A960582";
  routerAbi = [
    {
      inputs: [
        {
          internalType: "contract IAggregationExecutor",
          name: "executor",
          type: "address",
        },
        {
          components: [
            {
              internalType: "contract IERC20",
              name: "srcToken",
              type: "address",
            },
            {
              internalType: "contract IERC20",
              name: "dstToken",
              type: "address",
            },
            {
              internalType: "address payable",
              name: "srcReceiver",
              type: "address",
            },
            {
              internalType: "address payable",
              name: "dstReceiver",
              type: "address",
            },
            {
              internalType: "uint256",
              name: "amount",
              type: "uint256",
            },
            {
              internalType: "uint256",
              name: "minReturnAmount",
              type: "uint256",
            },
            {
              internalType: "uint256",
              name: "flags",
              type: "uint256",
            },
          ],
          internalType: "struct GenericRouter.SwapDescription",
          name: "desc",
          type: "tuple",
        },
        {
          internalType: "bytes",
          name: "permit",
          type: "bytes",
        },
        {
          internalType: "bytes",
          name: "data",
          type: "bytes",
        },
      ],
      name: "swap",
      outputs: [
        {
          internalType: "uint256",
          name: "returnAmount",
          type: "uint256",
        },
        {
          internalType: "uint256",
          name: "spentAmount",
          type: "uint256",
        },
      ],
      stateMutability: "payable",
      type: "function",
    },
  ];

  constructor(chainId: number) {
    console.log("initializing");
    this.chainId = chainId;
    this.broadcastApiUrl =
      "https://tx-gateway.1inch.io/v1.1/" + this.chainId + "/broadcast";
    this.apiBaseUrl = "https://api.1inch.io/v5.0/" + this.chainId;
  }

  private apiRequestUrl = (methodName: string, queryParams: any) => {
    return (
      this.apiBaseUrl +
      methodName +
      "?" +
      new URLSearchParams(queryParams).toString()
    );
  };

  public getTokens = async () => {
    const response = await fetch(this.apiRequestUrl("/tokens", {}), {
      method: "GET",
      headers: { "Content-Type": "application/json" },
    });
    const json = await response.json();
    const tokens = json.tokens as TokenRes;
    let tokenList: Token[] = [];
    for (const key in tokens) {
      tokenList.push(tokens[key]);
    }
    return tokenList;
  };

  public checkAllowance = async (
    tokenAddress: string,
    walletAddress: string
  ) => {
    const response = await fetch(
      this.apiRequestUrl("/approve/allowance", {
        tokenAddress,
        walletAddress,
      })
    );
    const json = await response.json();
    return json.allowance;
  };

  private broadCastRawTransaction = async (rawTransaction: string) => {
    const response = await fetch(this.broadcastApiUrl, {
      method: "post",
      body: JSON.stringify({ rawTransaction }),
      headers: { "Content-Type": "application/json" },
    });
    const json = await response.json();
    return json.transactionHash;
  };

  public signAndSendTransaction = async (
    wallet: ethers.Wallet,
    transaction: ethers.TransactionRequest
  ) => {
    const signed = await wallet.signTransaction(transaction);
    return signed;
    //   return await broadCastRawTransaction(signed);
  };
  signAndSendTransactionMeta = async (
    signer: ethers.Signer,
    transaction: ethers.TransactionRequest
  ) => {
    const rawTransaction = await signer.signTransaction(transaction);
    return await this.broadCastRawTransaction(rawTransaction);
  };

  public buildTxForApprove = async (
    wallet: ethers.Wallet,
    tokenAddress: string,
    amount: string
  ) => {
    const queryParams = amount ? { tokenAddress, amount } : { tokenAddress };
    const url = this.apiRequestUrl("/approve/transaction", queryParams);
    const response = await fetch(url);
    const transaction = await response.json();

    const gasLimit = await wallet.estimateGas({
      ...transaction,
      from: wallet.address,
    });

    return {
      ...transaction,
      gasLimit,
    };
  };

  public buildTxForSwap = async (swapParams: any) => {
    const url = this.apiRequestUrl("/swap", swapParams);

    const result = await fetch(url);
    const json = await result.json();

    return json.tx;
  };

  public approve = async (provider: ethers.Provider, wallet: ethers.Wallet) => {
    // const transactionForSign = await buildTxForApprove(
    //   swapParams.fromTokenAddress
    // );
    // transactionForSign.gasLimit = transactionForSign.gasLimit.toString();
    const nonce = await provider.getTransactionCount(wallet.address);
    const transactionForSign = {
      chainId: this.chainId,
      nonce: nonce,
      data: "0x095ea7b30000000000000000000000001111111254eeb25477b68fb85ed929f73a960582ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff",
      gasPrice: "3500000000",
      gas: 2000000,
      to: "0x1af3f329e8be154074d8769d1ffa4ee058b1dbc3",
      value: "0",
      gasLimit: 29406,
      chain: "mainnet",
    };
    const approveTx = await this.signAndSendTransaction(
      wallet,
      transactionForSign
    );
    console.log("approve tx hash", approveTx);
  };

  public swap = async (wallet: ethers.Wallet) => {
    const swapParams = {
      fromTokenAddress: "0x1af3f329e8be154074d8769d1ffa4ee058b1dbc3", // DAI
      toTokenAddress: "0x111111111117dc0aa78b770fa6a738034120c302", // 1INCH
      amount: "100000000000000000",
      fromAddress: wallet.address,
      slippage: 1,
      disableEstimate: false,
      allowPartialFill: false,
    } as ethers.TransactionRequest;

    const swapTransaction = await this.buildTxForSwap(swapParams);
    console.log("Transaction for swap: ", swapTransaction);

    const swapTxHash = await this.signAndSendTransaction(wallet, swapParams);
    console.log("Swap transaction hash: ", swapTxHash);
  };

  public swapMeta = async (
    signer: ethers.Signer,
    inToken: string,
    outToken: string,
    amount: string
  ) => {
    const swapParams = {
      fromTokenAddress: inToken,
      toTokenAddress: outToken,
      amount: amount,
      fromAddress: await signer.getAddress(),
      slippage: 1,
      disableEstimate: false,
      allowPartialFill: false,
    };

    const builtSwap = await this.buildTxForSwap(swapParams);
    const routerContract = new ethers.Contract(
      this.routerAddress,
      this.routerAbi,
      signer
    );
    const decodedSwap = routerContract.interface.decodeFunctionData(
      "swap",
      builtSwap.data
    );

    const executor = decodedSwap[0];
    const descArr = decodedSwap[1];
    const permit = decodedSwap[2];
    const data = decodedSwap[3];

    const desc = {
      srcToken: descArr[0],
      dstToken: descArr[1],
      srcReceiver: descArr[2],
      dstReceiver: descArr[3],
      amount: descArr[4],
      minReturnAmount: descArr[5],
      flags: descArr[6],
    };
    console.log(executor, desc, permit, data);

    const swapTx = await routerContract.swap(executor, desc, permit, data);

    console.log("swap tx hash:", swapTx.hash);
    await swapTx.wait();

    console.log("Swap successful!");
  };
}
