import type { NextPage } from "next";
import Head from "next/head";
import styles from "../styles/Home.module.css";
import Link from "next/link";

const Home: NextPage = () => {
  return (
    <div className={styles.container}>
      <Head>
        <title>Create Next App</title>
        <meta name="description" content="dex aggregators" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>Dex Aggregators</h1>

        <p className={styles.description}>hello, this is aggregator example</p>

        <div className={styles.grid}>
          <Link href="/1inch" className={styles.card}>
            <h2>1inch&rarr;</h2>
            <p>find more about 1inch aggregator example</p>
          </Link>

          <Link href="/openocean" className={styles.card}>
            <h2>Open ocean&rarr;</h2>
            <p>find more about open ocean aggregator example</p>
          </Link>
        </div>
      </main>

      <footer className={styles.footer}>Powered by me</footer>
    </div>
  );
};

export default Home;
