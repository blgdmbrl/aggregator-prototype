import { OpenoceanApiSdk } from "@openocean.finance/api";
import { ReqSwapVo } from "@openocean.finance/api/lib/swapSdk/Swap";
import { useEffect, useState } from "react";
import { OpenOceanLib } from "../../libs/openocean";

const OpenoceanPage = () => {
  const [chain, setChain] = useState("bsc");
  const [tokens, setTokens] = useState<any[]>([]);
  const [inToken, setInToken] = useState<any>();
  const [outtoken, setOutToken] = useState<any>();
  const [walletName, setWalletName] = useState("MetaMask");
  const [loading, setLoading] = useState(false);
  const [openLib, setOpenLib] = useState<OpenOceanLib>(
    new OpenOceanLib(chain, walletName)
  );

  useEffect(() => {
    const getTokens = async () => {
      const tokens = await openLib.getTokenList();
      setTokens([...tokens]);
    };
    if (openLib) {
      getTokens();
    }
    if (openLib && openLib.getWallet()) {
      return;
    }
    openLib.connectWallet();
  }, [openLib]);

  const swap = async () => {
    try {
      setLoading(true);
      const gasPrice = await openLib.getGasPrice();
      const amount = 1,
        slippage = 1;
      const quote = await openLib.getQuote(
        inToken.address,
        outtoken.address,
        amount,
        gasPrice,
        slippage
      );
      console.log(quote);
      await openLib.swap({
        chain,
        inTokenAddress: inToken.address,
        outTokenAddress: outtoken.address,
        amount,
        slippage,
        account: openLib.getWallet().address,
        gasPrice,
      });
    } catch (err) {
      alert(err);
      console.log("swap err:", err);
    } finally {
      setLoading(false);
    }
  };
  return (
    <>
      <div className="container mx-auto">
        <div className="flex flex-col items-center justify-center mt-10">
          <h1 className="text-4xl">OpenOcean example</h1>
          <div className="flex mt-20">
            <div className="flex">
              <div className="h-[80vh] overflow-auto">
                {tokens.map((token) => {
                  return (
                    <button
                      className="flex p-2"
                      onClick={() => setInToken(token)}
                    >
                      <h1>{token?.name}</h1>
                      <p>({token?.symbol})</p>
                    </button>
                  );
                })}
              </div>

              <div className="h-[80vh] overflow-auto">
                {tokens.map((token) => {
                  return (
                    <button
                      className="flex p-2"
                      onClick={() => setOutToken(token)}
                    >
                      <h1>{token?.name}</h1>
                      <p>({token?.symbol})</p>
                    </button>
                  );
                })}
              </div>
            </div>
            <div>
              {outtoken?.symbol}: {inToken?.symbol}
              <button
                className="p-2 border ml-2"
                onClick={swap}
                disabled={loading}
              >
                {!loading ? <p>swap</p> : <p>loading</p>}
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default OpenoceanPage;
