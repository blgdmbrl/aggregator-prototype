import { useEffect, useState } from "react";
import { OneinchLib, Token } from "../../libs/oneInch";
import { ethers, BrowserProvider } from "ethers";

const OneInchPage = () => {
  const oneInch = new OneinchLib(56);
  const [tokens, setTokens] = useState<Token[]>([]);
  const [inToken, setInToken] = useState<Token>();
  const [outToken, setOutToken] = useState<Token>();
  const [loading, setLoading] = useState(false);
  const [signer, setSigner] = useState<ethers.Signer>();
  const connectToMetamask = async () => {
    console.log("hey");
    const provider = new BrowserProvider(window.ethereum);
    const accounts = await provider.send("eth_requestAccounts", []);
    const signer = await provider.getSigner(accounts[0]);
    setSigner(signer);
  };

  useEffect(() => {
    (async () => {
      const tokens = await oneInch.getTokens();
      setTokens(tokens);
    })();
    connectToMetamask();
  }, []);

  const swap = async () => {
    try {
      setLoading(true);
      if (!signer || !inToken || !outToken) return;

      const amount = "100000000000";
      const dai = '0x1af3f329e8be154074d8769d1ffa4ee058b1dbc3'
      const inch = "0x111111111117dc0aa78b770fa6a738034120c302";
      await oneInch.swapMeta(
        signer,
        // inToken?.address,
        dai,
        // outToken?.address,
        inch,
        amount
      );
    } catch (err) {
    } finally {
      setLoading(false);
    }
  };
  return (
    <>
      <div className="container mx-auto">
        <div className="flex flex-col items-center justify-center mt-10">
          <h1 className="text-4xl">OpenOcean example</h1>
          <div className="flex mt-20">
            <div className="flex">
              <div className="h-[80vh] overflow-auto">
                {tokens.map((token) => {
                  return (
                    <button
                      className="flex p-2"
                      onClick={() => setInToken(token)}
                    >
                      <h1>{token.name}</h1>
                      <p>({token.symbol})</p>
                    </button>
                  );
                })}
              </div>

              <div className="h-[80vh] overflow-auto">
                {tokens.map((token) => {
                  return (
                    <button
                      className="flex p-2"
                      onClick={() => setOutToken(token)}
                    >
                      <h1>{token.name}</h1>
                      <p>({token.symbol})</p>
                    </button>
                  );
                })}
              </div>
            </div>
            <div>
              {outToken?.symbol}: {inToken?.symbol}
              <button className="p-2 border ml-2" onClick={swap}>
                {!loading ? <p>swap</p> : <p>loading</p>}
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default OneInchPage;
